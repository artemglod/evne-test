<?php
    function last_4_films_shortcode() {
        $query = new WP_Query( array(
            'post_type' => 'film',
            'posts_per_page' => 4,
        ) );

        $output = '<div class="container">';
            $output .= '<div class="row">';

                while($query->have_posts()) : $query->the_post();

                    $output .= '<article class="col-12 col-sm-6 col-xl-3 mb-4">';

                        $output .= '<div class="post-poster">';
                            $poster = get_field('poster');

                            $output .= '<img class="mx-auto" src="' . $poster['sizes']['large'] . '" alt="' . $poster['alt'] . '" />';
                        $output .= '</div>';

                        $output .= '<div class="post-details">';
                            $output .= '<h2 class="mt-3">';
                                $output .= '<a href="' . get_the_permalink() . '">';
                                    $output .= get_the_title();
                                $output .= '</a>';
                            $output .= '</h2>';

                            $output .= '<ul class="ml-0">';
                                $output .= '<li>';
                                    $output .= '<strong>';
                                        $output .= __('Стоимость сеанса: ');
                                    $output .= '</strong>';
                                    $output .= get_field('session_cost');
                                $output .= '</li>';

                                $output .= '<li>';
                                    $output .= '<strong>';
                                        $output .= __('Дата выхода: ');
                                    $output .= '</strong>';
                                    $output .= get_field('release_date');
                                $output .= '</li>';
                            $output .= '</ul>';
                        $output .= '</div>';

                    $output .= '</article>';

                endwhile;
                wp_reset_postdata();

            $output .= '</div>';
        $output .= '</div>';

        return $output;

    }
    add_shortcode('last-4-films', 'last_4_films_shortcode');