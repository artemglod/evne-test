<?php
/**
 * Template Name: Home page
 */

get_header();
?>

<main id="site-content" role="main">

    <?php
        $query = new WP_Query( array(
            'post_type' => 'film',
            'posts_per_page' => -1,
        ) );

        if ( $query->have_posts() ) {

            while ( $query->have_posts() ) {
                $query->the_post();

                get_template_part( 'template-parts/content-film' );
            }
            wp_reset_postdata();
        }
    ?>

</main><!-- #site-content -->

<?php get_footer(); ?>
