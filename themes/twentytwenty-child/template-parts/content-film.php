<?php $filmID = get_the_ID(); ?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="post-inner">

        <div class="container">
            <div class="row">

                <div class="col-12 col-md-6">
                    <?php
                        $poster = get_field('poster');
                        if (is_singular('film')) {
                            $posterIMG = $poster['url'];
                        } else {
                            $posterIMG = $poster['sizes']['large'];
                        }
                    ?>
                    <img class="mx-auto" src="<?php echo $posterIMG; ?>" alt="<?php echo $poster['alt'] ?>" />
                </div>

                <div class="col-12 col-md-6">
                    <?php if (is_singular('film')) { ?>
                        <h1>
                            <?php the_title(); ?>
                        </h1>
                    <?php } else { ?>
                        <h2 class="mt-3 mt-md-0">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>
                        </h2>
                    <?php } ?>
                    <ul class="ml-0">
                        <li>
                            <strong>
                                <?php _e('Стоимость сеанса: '); ?>
                            </strong>
                            <?php the_field('session_cost'); ?>
                        </li>
                        <li>
                            <strong>
                                <?php _e('Дата выхода: '); ?>
                            </strong>
                            <?php the_field('release_date'); ?>
                        </li>

                        <?php
                            if (is_singular('film')) {
                                $genre_terms = get_the_terms($filmID, 'genre');  // country, year, actor
                                if (is_array($genre_terms)) {
                        ?>
                                    <li>
                                        <strong>
                                            <?php _e('Жанры: '); ?>
                                        </strong>
                                        <?php
                                        $num = 1;
                                        $count = count($genre_terms);

                                        foreach ($genre_terms as $genre_term) {
                                            echo '<a href="' . get_term_link($genre_term->term_id, $genre_term->taxonomy) . '">' . mb_strtolower($genre_term->name) . '</a>';
                                            echo ($num == $count) ? '' : ', ';

                                            $num++;
                                        }
                                        ?>
                                    </li>
                            <?php } ?>

                            <?php
                                $genre_terms = get_the_terms($filmID, 'country');  // country, year, actor
                                if (is_array($genre_terms)) {
                            ?>
                                    <li>
                                        <strong>
                                            <?php _e('Страна: '); ?>
                                        </strong>
                                        <?php
                                        foreach ($genre_terms as $genre_term) {
                                            echo '<a href="' . get_term_link($genre_term->term_id, $genre_term->taxonomy) . '">' . $genre_term->name . '</a>';
                                        }
                                        ?>
                                    </li>
                            <?php } ?>

                            <?php
                                $genre_terms = get_the_terms($filmID, 'year');  // country, year, actor
                                if (is_array($genre_terms)) {
                            ?>
                                    <li>
                                        <strong>
                                            <?php _e('Год: '); ?>
                                        </strong>
                                        <?php
                                        foreach ($genre_terms as $genre_term) {
                                            echo '<a href="' . get_term_link($genre_term->term_id, $genre_term->taxonomy) . '">' . $genre_term->name . '</a>';
                                        }
                                        ?>
                                    </li>
                            <?php } ?>

                            <?php
                                $genre_terms = get_the_terms($filmID, 'actor');
                                if (is_array($genre_terms)) {
                            ?>
                                    <li>
                                        <strong>
                                            <?php _e('Актеры: '); ?>
                                        </strong>
                                        <?php
                                            $num = 1;
                                            $count = count($genre_terms);

                                            foreach ($genre_terms as $genre_term) {
                                                echo '<a href="' . get_term_link($genre_term->term_id, $genre_term->taxonomy) . '">' . $genre_term->name . '</a>';
                                                echo ($num == $count) ? '' : ', ';

                                                $num++;
                                            }
                                        ?>
                                    </li>
                        <?php
                                }
                            }
                        ?>
                    </ul>
                </div>

            </div>
        </div>

	</div><!-- .post-inner -->

	<?php
        if ( is_single() ) {

            get_template_part( 'template-parts/navigation' );

        }
	?>

</article><!-- .post -->
