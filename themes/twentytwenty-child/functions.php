<?php
    //add parent style
    function twentytwenty_child_theme_scripts() {
        wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
        wp_enqueue_style( 'bootstrap-style', '//stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' );
    }
    add_action( 'wp_enqueue_scripts', 'twentytwenty_child_theme_scripts' );

    //add fields for cpt film
    get_template_part( 'inc/acf_fields-film' );

    //add shortcode - 4 last films
    get_template_part( 'inc/shortcode-last-films' );
